Arg-Pooling
===========

Project for college's subject "Interfaces Adaptadas a Dispotivos Móviles".

Demo available at [RedHat cloud](http://argpooling-yukamustang.rhcloud.com/).

To do
-----
* Set date, time and frequency of pooling.
* Offer or ask for rides.
* Restrintions on editions and deletions.
* ...
* Bug fixes and enhancements!
