# -*- coding: utf-8 -*-
__author__ = 'mkaminose'

from django import template

register = template.Library()


@register.simple_tag
def active_url(request, pattern):
    if request.path.startswith(pattern):
        return 'active'
    return ''