__author__ = 'yukakami'

import json
from django.http import HttpResponse
from django.views.generic.detail import SingleObjectMixin
from django.contrib.auth.decorators import login_required
from django.utils.translation import ugettext as _
from django.http import Http404
from django.utils.decorators import method_decorator


class LoginRequiredView(object):

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(LoginRequiredView, self).dispatch(request, *args, **kwargs)


class OwnerRestrictedView(SingleObjectMixin):

    def get_object(self, queryset=None):
        obj = super(OwnerRestrictedView, self).get_object(queryset)
        if obj and obj.owner != self.request.user:
            raise Http404(_("you don't have permission to access this page").capitalize())
        return obj


class JsonResponseMixin(object):
    """
    A mixin that can be used to render a JSON response.
    """
    def render_to_json_response(self, context, **response_kwargs):
        """
        Returns a JSON response, transforming 'context' to make the payload.
        """
        return HttpResponse(self.convert_context_to_json(context), content_type='application/json', **response_kwargs)

    def convert_context_to_json(self, context):
        """ Convert the context dictionary into a JSON object """
        # Note: This is *EXTREMELY* naive; in reality, you'll need
        # to do much more complex handling to ensure that arbitrary
        # objects -- such as Django model instances or querysets
        # -- can be serialized as JSON.
        return json.dumps(context)