from django.forms.widgets import DateInput
from django.conf import settings


class DateWidget(DateInput):

    def __init__(self, *args, **kwargs):
        attrs = kwargs.get('attrs', {})
        classattrs = attrs.get('class', '')
        classattrs += ' date-widget'
        attrs['class'] = classattrs
        date_format = getattr(settings, 'DATE_FORMAT_PLACEHOLDER', 'dd-mm-yyyy')
        attrs['data-format'] = date_format
        attrs['placeholder'] = date_format
        kwargs['attrs'] = attrs
        super(DateWidget, self).__init__(*args, **kwargs)