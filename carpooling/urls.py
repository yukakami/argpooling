from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.auth.views import password_reset, password_reset_done, password_reset_confirm
from django.contrib.auth.views import password_reset_complete
from django.views.generic.base import RedirectView
from tastypie.api import Api
from region.api import CityResource
from pooling.views import MyLocationListView, MyPoolingListView, IndexView
from member.views import MemberUpdateView

admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^admin/', include(admin.site.urls)),
                       )

urlpatterns += patterns('',
                        url(r'^members/', include('member.urls', namespace='member')),
                        url(r'^poolings/', include('pooling.urls', namespace='pooling')), )

urlpatterns += patterns('',
                        url(r'^regions/', include('region.urls', namespace='region')),)

login_urls = patterns('',
                      url(r'^login/', RedirectView.as_view(url='/account/login/')),
                      url(r'^logout/', RedirectView.as_view(url='/account/logout/')),
                      url(r'^signup/', RedirectView.as_view(url='/account/signup/')), )

reset_password_urls = patterns('',
                               url(r'^password_reset/$', password_reset, name='password_reset'),
                               url(r'^password_reset/done/$', password_reset_done, name="password_reset_done"),
                               url(r'^password_reset/confirm/(?P<uidb36>[0-9A-Za-z]{1,13})-(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
                                   password_reset_confirm, name="password_reset_confirm"),
                               url(r'^password_reset/complete/$', password_reset_complete, name="password_reset_complete"))

my_locations_urls = patterns('',
                             url(r'^my_locations/$', MyLocationListView.as_view(), name='my_locations'),
                             url(r'^my_poolings/$', MyPoolingListView.as_view(), name='my_poolings'),)

account_urls = patterns('',
                        url(r'^account/', include(my_locations_urls, namespace='account')),
                        url(r'^account/', include('allauth.urls')),
                        url(r'^account/profile/', MemberUpdateView.as_view(), name='account_profile'), )
                        # url(r'^accounts/', include('userena.urls')), )

urlpatterns += login_urls + reset_password_urls + account_urls

api = Api(api_name='v1')
api.register(CityResource())

urlpatterns += patterns('', url(r'^api/', include(api.urls)), ) + \
               patterns('', url(r'^$', IndexView.as_view(), name='index'))
