var http = require('http');
var server = http.createServer().listen(3000, 'localhost');
var io = require('socket.io').listen(server);
var redis = require('socket.io/node_modules/redis').createClient();

io.configure(function() {
    // configurations
});

redis.psubscribe('notify_*'); // subscribe by pattern

io.sockets.on('connection', function(socket) {
   redis.on('pmessage', function(pattern, channel, message) {
       socket.emit(channel, message);
   });
});