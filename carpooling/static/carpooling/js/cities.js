$(".input_city").each(function(input) {
    $(input).autocomplete({
        serviceUrl: '/api/v1/city',
        onSelect: function(suggestion) {
            alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
        }
    });
});
$('#id_city_text').autocomplete({
        serviceUrl: '/regions/cities/json-list/', //Django.url('pooling:location:json-list'),
        minChars: 5,
        paramName: 'q',
        onSelect: function(selected_suggestion) {
            $("#id_city").val(selected_suggestion.data);
        }
    });
