// http://bootstrap-datepicker.readthedocs.org/en/release/
$(document).ready(function() {
    $(".date-widget").each(function() {
        $(this).datepicker({
            format: $(".date-widget").attr("data-format"),
            startDate: "today",
            autoclose: true
        })
    });
});

// poner que si el input tiene un valor, hacer el startDate desde ese día, sino desde 'today'