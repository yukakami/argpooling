"""
Django settings for carpooling project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
ON_SERVER = 'OPENSHIFT_APP_NAME' in os.environ


def path(filename):
    return '%s/%s' % (BASE_DIR, filename)


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '$cao^+((=c-+$zu#hp(5#h7jyb%tt*c^tj#oyjzg)-^!kfft2h'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = not ON_SERVER

TEMPLATE_DEBUG = not ON_SERVER

ALLOWED_HOSTS = ['localhost', 'argpooling-yukamustang.rhcloud.com']


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.contrib.gis',
    'south',
    'requests',
    'tastypie',
    'widget_tweaks',
    'redis',
    'allauth', 'allauth.account', 'allauth.socialaccount',
    'allauth.socialaccount.providers.facebook',
    'allauth.socialaccount.providers.twitter',
    # 'accounts',
    'member',
    'region',
    'pooling',
    'generic',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'generic.middleware.threadlocal.ThreadLocalMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.request",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",
    "pooling.context_processors.applications_counter",
    # allauth specific context processors
    "allauth.account.context_processors.account",
    "allauth.socialaccount.context_processors.socialaccount", )

AUTHENTICATION_BACKENDS = (
    # Needed to login by username in Django admin, regardless of `allauth`
    "django.contrib.auth.backends.ModelBackend",
    # 'guardian.backends.ObjectPermissionBackend',
    # 'django.contrib.auth.backends.ModelBackend',

    # `allauth` specific authentication methods, such as login by e-mail
    "allauth.account.auth_backends.AuthenticationBackend",
)

ROOT_URLCONF = 'carpooling.urls'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases
import sys
if 'test' in sys.argv:
    DATABASES = {
        'default': {
            'ENGINE': 'django.contrib.gis.db.backends.spatialite',
            'NAME': 'carpooling'
        },
    }
elif ON_SERVER:
    DATABASES = {
        'default': {
            'ENGINE': 'django.contrib.gis.db.backends.postgis',
            'NAME': os.environ['OPENSHIFT_APP_NAME'],
            'USER': os.environ['OPENSHIFT_POSTGRESQL_DB_USERNAME'],
            'PASSWORD': os.environ['OPENSHIFT_POSTGRESQL_DB_PASSWORD'],
            'HOST': os.environ['OPENSHIFT_POSTGRESQL_DB_HOST'],
            'PORT': os.environ['OPENSHIFT_POSTGRESQL_DB_PORT']
        }
    }
else:
    DATABASES = {
        'default': {
            'ENGINE': 'django.contrib.gis.db.backends.postgis',
            # 'ENGINE': 'django.db.backends.mysql',
            'NAME': 'carpooling',
            'USER': 'yukamustang',
            'PASSWORD': 'carpooling',
            'HOST': '127.0.0.1',
            'PORT': '',
        },
    }

POSTGIS_VERSION = (2, 1, 1)

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'es'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = False

USE_TZ = True

DATE_FORMAT = 'd-m-Y'

DATE_INPUT_FORMATS = ('%d-%m-%Y', '%Y-%m-%d', '%d/%m/%Y', '%Y/%m/%d', )

DATE_FORMAT_PLACEHOLDER = 'dd-mm-yyyy'

SITE_NAME = 'Arg-Pooling'

LOCALE_PATHS = (
    path('member/locale'), path('pooling/locale'), path('carpooling/locale'), path('conf/locale')
)

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
if ON_SERVER:
    STATIC_ROOT = os.path.join(os.environ['OPENSHIFT_REPO_DIR'], 'wsgi', 'static')
else:
    STATIC_ROOT = path('staticfiles')

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

STATICFILES_DIRS = (
    path('carpooling/static'),
)

TEMPLATE_DIRS = (
    path('carpooling/templates'),
    path('member/templates'), path('pooling/templates'), path('carpooling/templates'),
    # '/home/angras11/VirtualEnvs/trash-env/lib/python2.7/site-packages/userena/templates'
)

PAGINATE_BY = 5

AUTH_USER_MODEL = 'member.Member'

LOGIN_REDIRECT_URL = '/poolings/list'

LOGIN_URL = '/login'

# django-allauth needs it
SITE_ID = 1

# django-allauth config.
ACCOUNT_SIGNUP_FORM_CLASS = 'member.forms.RegistrationForm'
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_EMAIL_VERIFICATION = ("none")
ACCOUNT_LOGOUT_ON_GET = True
ACCOUNT_USERNAME_MIN_LENGTH = 4
EMAIL_VERIFICATION = 'never'
# https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-SESSION_SERIALIZER
SESSION_SERIALIZER = 'django.contrib.sessions.serializers.PickleSerializer'

# EMAIL_BACKEND = 'django.core.mail.backends.dummy.EmailBackend'
#
# EMAIL_USE_TLS = True
# EMAIL_HOST = 'smtp.gmail.com'
# EMAIL_PORT = 587
# EMAIL_HOST_USER = 'yourgmailaccount@gmail.com'
# EMAIL_HOST_PASSWORD = 'yourgmailpassword'
#
# ANONYMOUS_USER_ID = -1
#
# AUTH_PROFILE_MODULE = 'accounts.MyProfile'
# LOGIN_REDIRECT_URL = '/accounts/%(username)s/'
# LOGIN_URL = '/accounts/signin/'
# LOGOUT_URL = '/accounts/signout/'

if ON_SERVER:
    NODE_HOST = 'carpoolingnotification-yukamustang.rhcloud.com'
    NODE_PORT = 3000
else:
    NODE_HOST = 'localhost'
    NODE_PORT = 3000

# test settings
SPATIALITE_SQL = path(os.path.join('conf', 'init_spatialite-2.3.sql'))
