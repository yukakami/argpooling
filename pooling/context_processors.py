from pooling.models import Application, ApplicationStatus
from django.db.models import Q
from django.conf import settings

def applications_counter(request):
    received_counter = Application.objects.filter(Q(pooling__member=request.user.pk),
                                                  Q(status=ApplicationStatus.PENDING)).count()
    pending_counter = Application.objects.filter(Q(member=request.user.pk), 
                                                 Q(status=ApplicationStatus.PENDING)).count()
    ongoing_counter = Application.objects.filter(Q(member=request.user.pk) | Q(pooling__member=request.user.pk),
                                                 Q(status=ApplicationStatus.ACCEPTED)).count()
    node_host = getattr(settings, 'NODE_HOST', 'localhost')
    node_port = getattr(settings, 'NODE_PORT', 3000)
    return {'ongoing_counter': ongoing_counter, 'received_counter': received_counter,
            'pending_counter': pending_counter, 'node_host': node_host, 'node_port': node_port}