__author__ = 'yukakami'

from django.db.models.signals import post_save
from django.utils.translation import ugettext as _
from django.utils.formats import date_format
from django.utils import timezone
from models import Application
import redis
import json


def notify_application(sender, instance, **kwargs):
    notify_member_id = None
    data = {}
    if instance.was_accepted():
        data['message'] = _("%s has accepted your application." % instance.pooling.member.get_name())
        data['status'] = 'accepted'
        notify_member_id = instance.member.id
    elif instance.was_rejected():
        data['message'] = _("%s has rejected your application." % instance.pooling.member.get_name())
        data['status'] = 'end'
        notify_member_id = instance.member.id
    elif instance.is_pending():
        data['message'] = _("You have received an application from %s." % instance.member.get_name())
        data['status'] = 'pending'
        notify_member_id = instance.pooling.member.id
    data['datetime'] = date_format(instance.last_status_change, 'DATETIME_FORMAT')
    r = redis.StrictRedis()
    r.publish('notify_%s' % notify_member_id, json.dumps(data))

post_save.connect(notify_application, sender=Application, dispatch_uid='notify_application')
