__author__ = 'yukakami'
import factory
from django.contrib.gis.geos import Point
import random
import datetime
from pooling.models import Location, LocationType, Pooling, Application, ApplicationStatus
import region.factories
import member.factories


class LocationFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Location

    name = factory.Iterator(LocationType.get_iterable())
    city = factory.SubFactory(region.factories.CityFactory)
    lnglat = Point(float(0), float(0))
    member = factory.SubFactory(member.factories.MemberFactory)
    description = factory.Sequence(lambda c: 'location_{0}'.format(c))


class PoolingFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Pooling

    location_from = factory.SubFactory(LocationFactory)
    location_to = factory.SubFactory(LocationFactory)
    date = datetime.datetime.now()
    description = factory.Sequence(lambda c: 'pooling_{0}'.format(c))
    member = factory.SubFactory(member.factories.MemberFactory)
    seats = random.randint(1, 4)


class ApplicationFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Application

    member = factory.SubFactory(member.factories.MemberFactory)
    pooling = factory.SubFactory(PoolingFactory)
    seats = random.randint(1, 4)
    creation_date = datetime.datetime.now()
    description = factory.Sequence(lambda c: 'application_{0}'.format(c))
