from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.forms import ModelForm, BooleanField, HiddenInput, CharField, ModelChoiceField, IntegerField, Form
from django.utils.translation import ugettext as _
from django.contrib.gis.geos import Point
from models import Location, Pooling, Application
from generic.widgets import DateWidget
from generic.middleware.threadlocal import get_current_user
from pooling.models import ApplicationStatus


class LocationForm(ModelForm):

    city_text = CharField()
    longitude = CharField(widget=HiddenInput)
    latitude = CharField(widget=HiddenInput)

    class Meta:
        model = Location
        exclude = ('member', 'lnglat', )
        widgets = {'latitude': HiddenInput, 'city': HiddenInput}

    def __init__(self, *args, **kwargs):
        super(LocationForm, self).__init__(*args, **kwargs)
        self.fields['city_text'].label = _("city".capitalize())
        self.fields['city_text'].widget.attrs['placeholder'] = _("City, Province, Country")
        if self.instance.lnglat:
            self.fields['latitude'].initial = self.instance.latitude
            self.fields['longitude'].initial = self.instance.longitude
        city = None
        try:
            city = self.instance.city
        except:
            pass
        finally:
            if city:
                self.fields['city_text'].initial = unicode(city)

    def clean(self):
        cleaned_data = super(LocationForm, self).clean()
        latitude = cleaned_data['latitude']
        longitude = cleaned_data['longitude']
        self.instance.lnglat = Point(float(longitude), float(latitude))
        return cleaned_data


class PoolingForm(ModelForm):

    class Meta:
        model = Pooling
        exclude = ('member', 'remaining_seats', 'public', 'time', )
        widgets = {'date': DateWidget}

    def __init__(self, *args, **kwargs):
        super(PoolingForm, self).__init__(*args, **kwargs)
        self.fields['location_from'].queryset = Location.objects.filter(member=get_current_user())
        self.fields['location_to'].queryset = Location.objects.filter(member=get_current_user())

    def clean(self):
        clean_data = super(PoolingForm, self).clean()
        if clean_data.get('location_from') is not None and \
                        clean_data.get('location_from') == clean_data.get('location_to'):
            msg = _('The starting location and destiny coincide.')
            self._errors['location_from'] = self.error_class([msg])
            del clean_data['location_from']
        return clean_data


class PoolingSearchForm(Form):
    location_from = ModelChoiceField(queryset=Location.objects.all())
    location_to = ModelChoiceField(queryset=Location.objects.all())
    max_distance_from = IntegerField(required=False)
    max_distance_to = IntegerField(required=False)
    in_need = BooleanField(required=False)

    def __init__(self, *args, **kwargs):
        current_user = kwargs.pop('current_user', get_current_user())
        super(PoolingSearchForm, self).__init__(*args, **kwargs)
        self.fields['location_from'].label = _("from").capitalize()
        self.fields['location_from'].queryset = Location.objects.filter(member=current_user)
        self.fields['location_to'].label = _("to").capitalize()
        self.fields['location_to'].queryset = Location.objects.filter(member=current_user)
        self.fields['max_distance_from'].label = _("maximum distance from").capitalize()
        self.fields['max_distance_from'].widget.attrs['min'] = 0
        self.fields['max_distance_from'].widget.attrs['placeholder'] = _("distance in meters").capitalize()
        self.fields['max_distance_to'].label = _("maximum distance to").capitalize()
        self.fields['max_distance_to'].widget.attrs['min'] = 0
        self.fields['max_distance_to'].widget.attrs['placeholder'] = _("distance in meters").capitalize()
        self.fields['in_need'].initial = True


class ApplicationForm(ModelForm):

    class Meta:
        model = Application
        exclude = ('member', 'creation_date', 'last_status_change', 'status_statistic', )
        widgets = {'pooling': HiddenInput}

    def __init__(self, *args, **kwargs):
        super(ApplicationForm, self).__init__(*args, **kwargs)
        try:
            if self.instance.end_flow():
                self.fields['seats'].widget.attrs['disabled'] = 'disabled'
                self.fields['description'].widget.attrs['disabled'] = 'disabled'
            else:
                self.fields['seats'].widget.attrs['max'] = self.instance.pooling.remaining_seats
                self.fields['seats'].widget.attrs['min'] = 1
        except ObjectDoesNotExist, err:
            print 'ApplicationForm: %s' % err
            pass
        try:
            if self.instance.status is None or self.instance.status and self.instance.id is None:
                del self.fields['status']
            elif self.instance.end_flow():
                self.fields['status'].widget.attrs['disabled'] = 'disabled'
            else:
                available_choices = self.instance.get_next_statuses()
                self.fields['status'].choices = available_choices
        except Exception, ex:
            print ex
            del self.fields['status']
            pass
        if self.instance.end_flow() or self.instance.was_accepted() or self.instance.status is not None and \
                self.instance.is_pending() and get_current_user().id != self.instance.member.id:
            del self.fields['seats']
            del self.fields['description']
        if self.instance.end_flow():
            del self.fields['status']

    def clean_status(self):
        status = self.cleaned_data['status'] if self.cleaned_data.has_key('status') else ApplicationStatus.PENDING
        print 'clean_status'
        print status
        return status

    def clean_seats(self):
        seats = self.cleaned_data['seats']
        status = self.cleaned_data['status'] if self.cleaned_data.has_key('status') else ApplicationStatus.PENDING
        if status != ApplicationStatus.PENDING and seats != self.instance.seats:
            raise ValidationError(_("changes are not allowed").capitalize())
        message = None
        pooling = self.cleaned_data['pooling']
        if seats > pooling.remaining_seats:
            message = "the number of seats cannot be greater than the pooling's"
        elif seats < 1:
            message = "invalid number of seats"
        if message:
            raise ValidationError(_(message).capitalize())
        return seats

    def clean_description(self):
        description = self.cleaned_data['description']
        status = self.cleaned_data['status'] if self.cleaned_data.has_key('status') else ApplicationStatus.PENDING
        if status != ApplicationStatus.PENDING and description != self.instance.description:
            raise ValidationError(_("changes are not allowed").capitalize())
        return description


class ApplicationInlineForm(ModelForm):

    class Meta:
        model = Application
        exclude = ('member', 'status', 'creation_date', 'last_status_change')
        widgets = {'pooling': HiddenInput}

    def __init__(self, *args, **kwargs):
        super(ApplicationInlineForm, self).__init__(*args, **kwargs)
        self.fields['seats'].widget.attrs['max'] = self.instance.pooling.seats
        self.fields['seats'].widget.attrs['min'] = 1
