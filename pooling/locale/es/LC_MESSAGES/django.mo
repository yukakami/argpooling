��    `        �         (  !   )     K     N  8   a  7   �     �     �  $   �  2   	     Q	     a	     q	     ~	     �	  +   �	     �	     �	  	   �	     �	     �	     
     
  	   
  	   
     (
     1
     I
     N
     T
     \
     h
     q
     
     �
     �
     �
     �
     �
     �
  	   �
     �
  
   �
     �
     �
       	          	        (     /     7     <     ?     C     V     _     v     �     �     �     �     �     �     �     �  	   �     �     �                    "     7     ?  !   F     h     q     z          �     �     �     �     �     �  -   �  -   �     (     -     0     5     >     J     W  C   [  k  �          '     *  4   ?  6   t     �     �  &   �  8   �     3     D     U     a     o  (   �     �     �     �     �     �     �     �            	         *     A     H     P     \  	   h     r     �     �     �     �     �     �     �     �     �     �               9  
   B     M     S     c     h     o     t     w          �     �     �     �     �     �  	   �                    '  	   *     4     ;     Q     c  	   i     s     �     �  "   �  	   �     �     �     �     �     �     �     �            .   '  -   V     �     �     �     �     �  
   �     �  :   �     _   ?   Z      A   S   /   -       =   1   )   I       &                         '   F      [   7   "       W          :   	   Y       B   U      X              $   *          !      5                0   
   C   K   6       %             T       N   D           (                 E   ,           @   #   8   9           2      M   \   +   3       J   ]              ^   <       R   .      P          L   V             O   >   `      ;       4                  G   H                   Q        %s has accepted your application. -- Applications' list Are you sure you want to remove the location %(object)s? Are you sure you want to remove the pooling %(object)s? City, Province, Country Delete pooling Indicates if its an offer or demand. Indicates the amount of seats needed or available. Location's list Locations' list New location New pooling Poolings' list The starting location and destiny coincide. accepted all received applications applicant application applications apply at available birthdate canceled changes are not allowed city clear college coordinates creation creation date date delete description distance in meters edit edit location edit pooling every day every month every week every weekday every weekend expired frequency from full name gender go back home id job last status change location location's description location's name maximum distance from maximum distance to name need never new location new pooling no no street offers on going applications ongoing poolings other pending pending applications pooling public received and pending applications rejected requires save school search seats see details status street there are no elements to show. there are no poolings that matches the search there are no seats available for this pooling time to user username view on map view profile yes you have already applied for this pooling. check the following link Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-04-08 20:19+0000
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 %s ha aceptado tu solicitud -- Lista de solicitudes Está seguro de querer eliminar el lugar %(object)s? Está seguro de querer eliminar el pooling %(object)s? Ciudad, Provincia, País Borrar pooling Indica si es una oferta o una demanda. Indica la cantidad de asientos necesarios o disponibles. Lista de lugares Lista de lugares Nuevo lugar Nuevo pooling Lista de poolings El lugar de comienzo y destino coinciden aceptado todas las solicitudes recibidas solicitudes solicitudes solicitudes aplicar a las disponibilidad cumpleaños cancelado no se permiten cambios ciudad limpiar universidad coordenadas creación fecha creación fecha borrar descripción distancia en metros editar editar lugar editar pooling todos los días todos los meses todas las semanas todos los días de semana todos los fines de semana expirado frecuencia desde nombre completo sexo volver casa id trabajo último cambio de estado lugar descripción del lugar nombre del lugar máxima distancia desde máxima distancia hasta nombre necesidad nunca nuevo lugar nuevo pooling no sin calle ofrece solicitudes en marcha poolings actuales otros pendiente solicitudes pendientes pooling público solicitudes recibidas y pendientes rechazado requiere guardar escuela buscar asientos ver detalles estado calle no hay lugares para mostrar no hay poolings que coincidan con la búsqueda no hay asientos disponibles para este pooling hora hasta usuario nombre de usuario ver en el mapa ver perfil si ya haz aplicado por este pooling. revisa el siguiente link 