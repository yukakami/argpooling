__author__ = 'yukakami'

from django.conf.urls import patterns, url, include
from views import *


location_urls = patterns('',
                         url(r'^new/$', LocationCreateView.as_view(), name='create'),
                         url(r'^update/(?P<pk>[0-9]+)/$', LocationUpdateView.as_view(), name='update'),
                         url(r'^delete/(?P<pk>[0-9]+)/$', LocationDeleteView.as_view(), name='delete'),
                         url(r'^json-detail/(?P<pk>[0-9]+)/$', LocationDetailJsonView.as_view(), name='json-detail'),
                         url(r'^list/$', LocationListView.as_view(), name='list'), )

pooling_urls = patterns('',
                        url(r'^(?P<pk>\d+)/$', PoolingDetailView.as_view(), name='detail'),
                        url(r'^new/$', PoolingCreateView.as_view(), name='create'),
                        url(r'^delete/(?P<pk>[0-9]+)/$', PoolingDeleteView.as_view(), name='delete'),
                        url(r'^search/$', PoolingSearchView.as_view(), name='search'),
                        url(r'^list/$', PoolingListView.as_view(), name='list'),
                        url(r'^json-detail/(?P<pk>[0-9]+)/$', PoolingDetailJsonView.as_view(), name='json-detail'),)

application_urls = patterns('',
                            url(r'^list/$', ApplicationListView.as_view(), name='my_applications'),
                            url(r'^received-detail/(?P<pk>\d+)/$', ApplicationReceivedUpdateView.as_view(), name='received-detail'),
                            url(r'^sent/(?P<pk>\d+)/$', ApplicationSentUpdateView.as_view(), name='sent'),
                            url(r'^received/$', ApplicationReceivedListView.as_view(), name='received_applications'), 
                            url(r'^ongoing/$', ApplicationOnGoingListView.as_view(), name='ongoing_applications'))

urlpatterns = patterns('',
                       url(r'^locations/', include(location_urls, namespace='location')),
                       url(r'^applications/', include(application_urls, namespace='application')),
                       url(r'', include(pooling_urls, namespace='pooling')), )
