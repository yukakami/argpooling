from django.views.generic import ListView, CreateView, DetailView, UpdateView, DeleteView, TemplateView, FormView
from django.conf import settings
from django.core.urlresolvers import reverse
from django.core.exceptions import MultipleObjectsReturned
from django.contrib.gis.measure import D  # `D` is shortcut for `Distance`
from django.db.models import Q
from generic.views import OwnerRestrictedView, JsonResponseMixin, LoginRequiredView
from models import *
from forms import *


class IndexView(TemplateView):
    template_name = 'base_front.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        return context


class LocationListView(ListView, LoginRequiredView):
    model = Location
    paginate_by = getattr(settings, 'PAGINATE_BY', 20)
    context_object_name = 'location_list'
    template_name = 'location/location_list.html'


class LocationCreateView(LoginRequiredView, CreateView):
    model = Location
    form_class = LocationForm
    template_name = 'location/location_form.html'

    def get_success_url(self):
        return reverse('account:my_locations')

    def form_valid(self, form):
        form.instance.member = self.request.user
        return super(LocationCreateView, self).form_valid(form)


class LocationDetailJsonView(JsonResponseMixin, TemplateView):

    def get_context_data(self, **kwargs):
        pk = kwargs.get('pk', None)
        if pk:
            location = Location.objects.get(pk=pk)
        else:
            location = None
        return {'latitude': location.latitude, 'longitude': location.longitude}

    def render_to_response(self, context, **response_kwargs):
        return self.render_to_json_response(context, **response_kwargs)


class LocationUpdateView(LoginRequiredView, OwnerRestrictedView, UpdateView):
    model = Location
    form_class = LocationForm
    template_name = 'location/location_form.html'

    def get_success_url(self):
        return reverse('account:my_locations')

    def form_valid(self, form):
        form.instance.member = self.request.user
        return super(LocationUpdateView, self).form_valid(form)


class LocationDeleteView(LoginRequiredView, OwnerRestrictedView, DeleteView):
    model = Location
    template_name = 'location/location_confirm_delete.html'

    def get_success_url(self):
        return reverse('account:my_locations')


class PoolingListView(LoginRequiredView, ListView):
    model = Pooling
    paginate_by = getattr(settings, 'PAGINATE_BY', 20)

    def get_queryset(self):
        return Pooling.objects.filter(public=True)

    def get_context_data(self, **kwargs):
        context_data = super(PoolingListView, self).get_context_data(**kwargs)
        object_list = context_data['object_list']
        application_forms = {}
        for pooling in object_list:
            application = Application(pooling=pooling, status=ApplicationStatus.PENDING)
            application_forms[pooling.id] = ApplicationInlineForm(instance=application)
        context_data['application_forms'] = application_forms
        return context_data


class PoolingCreateView(LoginRequiredView, CreateView):
    model = Pooling
    form_class = PoolingForm

    def get_success_url(self):
        return reverse('account:my_poolings')

    def form_valid(self, form):
        form.instance.member = self.request.user
        return super(PoolingCreateView, self).form_valid(form)


class PoolingDetailView(LoginRequiredView, FormView, DetailView):
    model = Pooling
    form_class = ApplicationForm

    def get(self, request, *args, **kwargs):
        self.object = Pooling.objects.get(pk=kwargs.get('pk'))
        if self.object.has_seats() and self.object.owner.id != request.user.id:
            applications = Application.objects.filter(member=request.user, pooling=self.object)
            if applications.count() > 0:
                applications = applications.exclude(Q(status=ApplicationStatus.CANCELED) |
                                                    Q(status=ApplicationStatus.EXPIRED) |
                                                    Q(status=ApplicationStatus.REJECTED))
            if applications.count() == 1:
                application = applications[0]
            else:
                application = Application(member=request.user, pooling=self.object)
            form = ApplicationForm(instance=application)
        else:
            form = None
        return self.render_to_response(self.get_context_data(form=form, object=self.object))

    def post(self, request, *args, **kwargs):
        self.object = Pooling.objects.get(pk=kwargs.get('pk'))
        if self.object.has_seats() and self.object.owner.id != request.user.id:
            applications = Application.objects.filter(member=request.user, pooling=self.object)
            if applications.count() > 0:
                applications = applications.exclude(Q(status=ApplicationStatus.CANCELED) |
                                                    Q(status=ApplicationStatus.EXPIRED) |
                                                    Q(status=ApplicationStatus.REJECTED))
            if applications.count() == 1:
                application = applications[0]
            else:
                application = Application(member=request.user, pooling=self.object)
        form = ApplicationForm(request.POST, instance=application)
        if form.is_valid():
            form.save(commit=True)
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_success_url(self):
        return reverse('pooling:application:my_applications')


class PoolingDeleteView(LoginRequiredView, OwnerRestrictedView, DeleteView):
    model = Pooling

    def get_success_url(self):
        return reverse('account:my_poolings')


class PoolingSearchView(LoginRequiredView, ListView):
    model = Pooling
    template_name = 'pooling/pooling_search.html'

    def get_context_data(self, **kwargs):
        context_data = super(PoolingSearchView, self).get_context_data(**kwargs)
        context_data['search_form'] = PoolingSearchForm(self.request.GET, current_user=self.request.user)
        return context_data

    def get_queryset(self):
        search_form = PoolingSearchForm(self.request.GET)
        if search_form.is_valid():
            location_from = search_form.cleaned_data.get('location_from')
            location_to = search_form.cleaned_data.get('location_to')
            in_need = search_form.cleaned_data.get('in_need')
            try:
                max_distance_from = search_form.cleaned_data['max_distance_from']
                if max_distance_from is None:
                    max_distance_from = 0
            except AttributeError:
                max_distance_from = 0
            try:
                max_distance_to = search_form.cleaned_data['max_distance_to']
                if max_distance_to is None:
                    max_distance_to = 0
            except AttributeError:
                max_distance_to = 0

            if location_from:
                filtered_locations_from = Location.objects.filter(lnglat__distance_lte=(location_from.lnglat,
                                                                                        D(m=max_distance_from)))\
                                                          .exclude(pk=location_from.pk)
            else:
                filtered_locations_from = []
            if location_to:
                filtered_locations_to = Location.objects.filter(lnglat__distance_lte=(location_to.lnglat,
                                                                                      D(m=max_distance_to)))\
                                                        .exclude(pk=location_to.pk)
            else:
                filtered_locations_to = []
            queryset = Pooling.objects.filter(in_need=in_need)\
                                      .filter(Q(location_from__in=filtered_locations_from) | Q(location_from__in=filtered_locations_to),
                                              Q(location_to__in=filtered_locations_from) | Q(location_to__in=filtered_locations_to))
        else:
            queryset = Pooling.objects.none()
        return queryset


class PoolingDetailJsonView(JsonResponseMixin, TemplateView):

    def get_context_data(self, **kwargs):
        pk = kwargs.get('pk', None)
        if pk:
            pooling = Pooling.objects.get(pk=pk)
        else:
            pooling = None
        return {'location_from_latitude': pooling.location_from.latitude,
                'location_from_longitude': pooling.location_from.longitude,
                'location_to_latitude': pooling.location_to.latitude,
                'location_to_longitude': pooling.location_to.longitude
                }

    def render_to_response(self, context, **response_kwargs):
        return self.render_to_json_response(context, **response_kwargs)


class ApplicationListView(LoginRequiredView, ListView):
    model = Application
    paginate_by = getattr(settings, 'PAGINATE_BY', 20)
    template_name = 'pooling/my_application_list.html'
    context_object_name = 'application_list'

    def get_queryset(self):
        return Application.objects.filter(Q(member=self.request.user.pk)).exclude(status=ApplicationStatus.PENDING)

    def get_context_data(self):
        context_data = super(ApplicationListView, self).get_context_data()
        context_data['pending_list'] = Application.objects.filter(Q(member=self.request.user.pk),
                                                                  Q(status=ApplicationStatus.PENDING))
        return context_data


class ApplicationReceivedListView(LoginRequiredView, ListView):
    model = Application
    paginate_by = getattr(settings, 'PAGINATE_BY', 20)
    template_name = 'pooling/my_received_application_list.html'
    context_object_name = 'received_application_list'

    def get_queryset(self):
        return Application.objects.filter(pooling__member=self.request.user.pk).exclude(status=ApplicationStatus.PENDING)

    def get_context_data(self, **kwargs):
        context_data = super(ApplicationReceivedListView, self).get_context_data(**kwargs)
        context_data['pending_list'] = Application.objects.filter(Q(pooling__member=self.request.user.pk),
                                                                               Q(status=ApplicationStatus.PENDING))
        return context_data


class ApplicationOnGoingListView(LoginRequiredView, ListView):
    model = Application
    paginate_by = getattr(settings, 'PAGINATE_BY', 20)
    template_name = 'pooling/my_ongoing_application_list.html'

    def get_queryset(self):
        return Application.objects.filter(Q(member=self.request.user.pk) | Q(pooling__member=self.request.user.pk),
                                          Q(status=ApplicationStatus.ACCEPTED))


class ApplicationCreateView(LoginRequiredView, CreateView):
    model = Application
    form_class = ApplicationForm

    def get_success_url(self):
        return reverse('pooling:application:my_applications')


class ApplicationReceivedUpdateView(LoginRequiredView, UpdateView):
    model = Application
    form_class = ApplicationForm
    template_name = 'pooling/application_detail_form.html'

    def get_success_url(self):
        return reverse('pooling:application:received_applications')

    def get_context_data(self, **kwargs):
        context_data = super(ApplicationReceivedUpdateView, self).get_context_data(**kwargs)
        context_data['application'] = Application.objects.get(pk=self.object.pk)
        return context_data


class ApplicationSentUpdateView(LoginRequiredView, OwnerRestrictedView, UpdateView):
    model = Application
    form_class = ApplicationForm
    template_name = 'pooling/application_sent_form.html'

    def get_success_url(self):
        return reverse('pooling:application:my_applications')

    def get_context_data(self, **kwargs):
        context_data = super(ApplicationSentUpdateView, self).get_context_data(**kwargs)
        context_data['application'] = Application.objects.get(pk=self.object.pk)
        return context_data


class MyLocationListView(LoginRequiredView, ListView):
    model = Location
    paginate_by = getattr(settings, 'PAGINATE_BY', 20)
    template_name = 'location/my_location_list.html'

    def get_queryset(self):
        return Location.objects.filter(member=self.request.user.pk)


class MyPoolingListView(LoginRequiredView, ListView):
    model = Pooling
    paginate_by = getattr(settings, 'PAGINATE_BY', 20)
    template_name = 'pooling/my_pooling_list.html'

    def get_queryset(self):
        return Pooling.objects.filter(member=self.request.user.pk)
