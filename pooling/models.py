from django.db import models
from django.contrib.gis.db import models as gis_models
from django.utils.translation import ugettext as _
from generic.middleware.threadlocal import get_current_user
import datetime

from region.models import City
from member.models import Member


class LocationType(object):
    HOME = 5
    SCHOOL = 10
    COLLEGE = 15
    JOB = 20
    OTHER = 100

    @classmethod
    def get_choices(cls):
        choices = [
            (cls.HOME, _('home'.capitalize())), (cls.SCHOOL, _('school'.capitalize())),
            (cls.COLLEGE, _('college'.capitalize())), (cls.JOB, _('job'.capitalize())),
            (cls.OTHER, _('other'.capitalize()))
        ]
        return choices

    @classmethod
    def get_name(cls, value):
        if value == cls.HOME:
            name = _('home'.capitalize())
        elif value == cls.SCHOOL:
            name = _('school'.capitalize())
        elif value == cls.COLLEGE:
            name = _('college'.capitalize())
        elif value == cls.JOB:
            name = _('job'.capitalize())
        else:
            name = _('other'.capitalize())
        return name

    @classmethod
    def get_iterable(cls):
        iterable = [cls.HOME, cls.SCHOOL, cls.COLLEGE, cls.JOB, cls.OTHER]
        return iterable


class Location(gis_models.Model):
    name = models.IntegerField(max_length=100, verbose_name=_('location\'s name'), choices=LocationType.get_choices())
    street = models.CharField(max_length=255, blank=True, null=True, verbose_name=_("street"))
    city = models.ForeignKey(City, verbose_name=_("city"))
    lnglat = gis_models.PointField(srid=4326)  # x = lng, y = lat
    description = models.TextField(blank=True, null=True, verbose_name=_('location\'s description'))
    member = models.ForeignKey(Member, default=get_current_user())
    objects = gis_models.GeoManager()

    def __unicode__(self):
        return u"%s: %s - %s" % (self.get_name(), unicode(self.city), self.street or _("no street"))

    @property
    def coordinates(self):
        return '%s, %s' % (str(self.latitude), str(self.longitude))

    @property
    def owner(self):
        return self.member

    @property
    def latitude(self):
        return self.lnglat.get_y()

    @property
    def longitude(self):
        return self.lnglat.get_x()

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        # self.member = get_current_user()
        super(Location, self).save()

    def get_name(self):
        return LocationType.get_name(self.name)


class PoolingFrequency(object):
    NEVER = 0
    EVERY_DAY = 1
    EVERY_WEEKDAY = 2
    EVERY_WEEKEND = 3
    EVERY_WEEK = 4
    EVERY_MONTH = 5

    @classmethod
    def get_choices(cls):
        choices = [(cls.NEVER, _('--')), (cls.EVERY_DAY, _('every day'.capitalize())),
                   (cls.EVERY_WEEKDAY, _('every weekday'.capitalize())), (cls.EVERY_WEEKEND, _('every weekend'.capitalize())),
                   (cls.EVERY_WEEK, _('every week'.capitalize())), (cls.EVERY_MONTH, _('every month'.capitalize()))]
        return choices

    @classmethod
    def get_name(cls, value):
        name = ''
        if value == cls.NEVER:
            name = _('never')
        elif value == cls.EVERY_DAY:
            name = _('every day'.capitalize())
        elif value == cls.EVERY_WEEKEND:
            name = _('every weekend'.capitalize())
        elif value == cls.EVERY_WEEK:
            name = _('every week'.capitalize())
        elif value == cls.EVERY_MONTH:
            name = _('every month'.capitalize())
        return name


class Pooling(models.Model):
    location_from = models.ForeignKey(Location, related_name='from_set', verbose_name=_("from"))
    location_to = models.ForeignKey(Location, related_name='to_set', verbose_name=_("to"))
    date = models.DateField(verbose_name=_("date"))
    time = models.TimeField(blank=True, null=True, verbose_name=_("time"))
    frequency = models.IntegerField(choices=PoolingFrequency.get_choices(), default=PoolingFrequency.NEVER,
                                    verbose_name=_("frequency"))
    description = models.TextField(blank=True, null=True, verbose_name=_('location\'s description'))
    public = models.BooleanField(blank=True, default=True, verbose_name=_("public"))
    member = models.ForeignKey(Member, default=get_current_user())
    in_need = models.BooleanField(blank=True, default=False, verbose_name=_("in need"),
                                  help_text=_('Indicates if its an offer or demand.'))
    seats = models.IntegerField(verbose_name=_("seats"), help_text=_('Indicates the amount of seats needed or available.'))
    remaining_seats = models.IntegerField()

    def __unicode__(self):
        return '%s - %s' % (unicode(self.location_from.city), unicode(self.location_to.city))

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        # self.member = get_current_user()
        if self.remaining_seats is None:
            self.remaining_seats = self.seats
        super(Pooling, self).save()

    @property
    def owner(self):
        return self.member

    def has_seats(self):
        return self.remaining_seats > 0

    def get_repeat(self):
        return PoolingFrequency.get_name(self.frequency)

    def occupy_seat(self, quant=1):
        self.remaining_seats -= quant
        # TODO check whether save needs to be called explicitly
        self.save()

    def release_seat(self, quant=1):
        self.remaining_seats += quant
        # TODO check whether save needs to be called explicitly
        self.save()


class ApplicationStatus(object):
    ACCEPTED = 1
    REJECTED = 5
    PENDING = 10
    CANCELED = 15
    EXPIRED = 20
    DELETED = 100

    @classmethod
    def get_name(cls, value):
        name = ''
        if value == cls.ACCEPTED:
            name = _('accepted'.capitalize())
        elif value == cls.REJECTED:
            name = _('rejected'.capitalize())
        elif value == cls.PENDING:
            name = _('pending'.capitalize())
        elif value == cls.CANCELED:
            name = _('canceled'.capitalize())
        elif value == cls.EXPIRED:
            name = _('expired'.capitalize())
        return name

    @classmethod
    def get_choices(cls):
        choices = [(cls.ACCEPTED, _('accepted').capitalize()), (cls.REJECTED, _('rejected').capitalize()),
                   (cls.PENDING, _('pending'.capitalize())), (cls.CANCELED, _('canceled'.capitalize())),
                   (cls.EXPIRED, _('expired'.capitalize()))]
        return choices

    @classmethod
    def next_1_statuses(cls):
        u""" ACCEPTED """
        choices = [cls.ACCEPTED, cls.CANCELED, cls.EXPIRED]
        # choices = [(cls.ACCEPTED, _('accepted').capitalize()), (cls.CANCELED, _('canceled'.capitalize())),
        #            (cls.EXPIRED, _('expired'.capitalize()))]
        return choices

    @classmethod
    def next_5_statuses(cls):
        u""" REJECTED """
        choices = [cls.REJECTED]
        # choices = [(cls.REJECTED, _('rejected'.capitalize()))]
        return choices

    @classmethod
    def next_10_statuses(cls):
        u""" PENDING """
        choices = [cls.PENDING, cls.ACCEPTED, cls.REJECTED, cls.CANCELED, cls.EXPIRED]
        # choices = [(cls.PENDING, _('pending'.capitalize())), (cls.ACCEPTED, _('accepted'.capitalize())),
        #            (cls.REJECTED, _('rejected'.capitalize())), (cls.CANCELED, _('canceled'.capitalize())),
        #            (cls.EXPIRED, _('expired'.capitalize()))]
        return choices

    @classmethod
    def next_10_applicant_statuses(cls):
        u""" PENDING, for the applicant """
        choices = [cls.PENDING, cls.CANCELED, cls.EXPIRED]
        # choices = [(cls.PENDING, _('pending'.capitalize())), (cls.CANCELED, _('canceled'.capitalize())),
        #            (cls.EXPIRED, _('expired'.capitalize()))]
        return choices

    @classmethod
    def next_10_receiver_statuses(cls):
        u""" PENDING, for the application receiver """
        choices = [cls.PENDING, cls.ACCEPTED, cls.REJECTED, cls.EXPIRED]
        # choices = [(cls.PENDING, _('pending'.capitalize())), (cls.ACCEPTED, _('accepted'.capitalize())),
        #            (cls.REJECTED, _('rejected'.capitalize())), (cls.CANCELED, _('canceled'.capitalize())),
        #            (cls.EXPIRED, _('expired'.capitalize()))]
        return choices

    @classmethod
    def next_15_statuses(cls):
        u""" CANCELED """
        choices = [cls.CANCELED]
        # choices = [(cls.CANCELED, _('canceled'.capitalize()))]
        return choices

    @classmethod
    def next_20_statuses(cls):
        u""" EXPIRED """
        choices = [cls.EXPIRED]
        # choices = [(cls.EXPIRED, _('expired'.capitalize()))]
        return choices


class Application(models.Model):
    member = models.ForeignKey(Member)
    pooling = models.ForeignKey(Pooling)
    seats = models.IntegerField(verbose_name=_("seats"))
    status = models.IntegerField(verbose_name=_("status"), choices=ApplicationStatus.get_choices(),
                                 default=ApplicationStatus.PENDING)
    status_statistic = models.IntegerField(choices=ApplicationStatus.get_choices(), null=True, blank=True)
    creation_date = models.DateTimeField(default=datetime.datetime.now(), verbose_name=_("creation date"))
    last_status_change = models.DateTimeField(default=datetime.datetime.now(), verbose_name=_("last status change"))
    description = models.TextField(blank=True, null=True, verbose_name=_("description"))

    _status = None

    class Meta:
        ordering = ['last_status_change', 'creation_date']

    def __init__(self, *args, **kwargs):
        super(Application, self).__init__(*args, **kwargs)
        self._status = self.status

    def __unicode__(self):
        return u"Pooling ID: %d, Pooler: %s, Applicant: %s" % (self.pooling.id, self.pooling.member, self.member)

    @property
    def owner(self):
        return self.member

    def get_status(self):
        return ApplicationStatus.get_name(self.status)

    def get_next_statuses(self):
        if self.status is None:
            _choices = ApplicationStatus.next_10_statuses()
        elif self.status == ApplicationStatus.PENDING:
            if get_current_user() == self.member:
                _choices = ApplicationStatus.next_10_applicant_statuses()
            else:
                _choices = ApplicationStatus.next_10_receiver_statuses()
        else:
            method_name = getattr(ApplicationStatus, 'next_%d_statuses' % self.status)
            _choices = method_name()
        choices = []
        for ch in _choices:
            choices.append((ch, ApplicationStatus.get_name(ch)))
        return choices

    def end_flow(self):
        return len(self.get_next_statuses()) == 1

    def is_pending(self):
        return self.status == ApplicationStatus.PENDING

    def was_rejected(self):
        return self.status == ApplicationStatus.REJECTED

    def was_accepted(self):
        return self.status == ApplicationStatus.ACCEPTED

    def was_canceled(self):
        return self.status == ApplicationStatus.CANCELED

    def has_expired(self):
        return self.status == ApplicationStatus.EXPIRED

    def was_sent_by_me(self):
        return get_current_user().id == self.member.id

    # def delete(self, using=None):
    #     self.status = ApplicationStatus.DELETED
    #     self.save(using=using)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.status != self._status:
            last_status_change = datetime.datetime.now()
            self.last_status_change = last_status_change
            if self.status == ApplicationStatus.ACCEPTED:
                self.pooling.occupy_seat(self.seats)
            elif self._status == ApplicationStatus.ACCEPTED and self.status == ApplicationStatus.CANCELED:
                # TODO shouldnt this be status in CANCELED, REJECTED, EXPIRED?
                self.pooling.release_seat(self.seats)
        if self._status == ApplicationStatus.PENDING and self.status != ApplicationStatus.PENDING:
            self.status_statistic = self.status
        super(Application, self).save()