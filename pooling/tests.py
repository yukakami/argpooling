# from django.test import TestCase
import unittest
import random
from django.test import Client
from django.core.exceptions import ValidationError
from pooling import factories
from pooling.models import ApplicationStatus
import member.factories


class PoolingTest(unittest.TestCase):

    def setUp(self):
        self.client = Client()
        user = member.factories.MemberFactory()
        login_status = self.client.login(username=user.username, password=user.username)
        if login_status:
            self.user = user
        else:
            self.fail("Login unsuccessful")

    def test_remaining_seats_on_create(self):
        pooling = factories.PoolingFactory(member=self.user)
        self.assertEquals(pooling.seats, pooling.remaining_seats)

    def test_occupy_seats(self):
        total_seats = random.randint(1, 4)
        pooling = factories.PoolingFactory(seats=total_seats, member=self.user)
        occupied_seats = 1
        pooling.occupy_seat(occupied_seats)
        self.assertGreaterEqual(pooling.remaining_seats, 0)
        self.assertEquals(pooling.remaining_seats, total_seats - occupied_seats)

    def test_release_seat(self):
        total_seats = random.randint(1, 4)
        pooling = factories.PoolingFactory(seats=total_seats, member=self.user)
        remaining_seats = pooling.remaining_seats
        released_seats = 1
        pooling.release_seat(released_seats)
        self.assertLessEqual(total_seats, pooling.remaining_seats)
        self.assertEquals(pooling.remaining_seats, remaining_seats + released_seats)

    def test_has_seats(self):
        pooling = factories.PoolingFactory(seats=random.randint(1, 4), member=self.user)
        self.assertTrue(pooling.has_seats())

    # def test_location_pooling_same_member(self):
    #     pooling = factories.PoolingFactory(member=self.user)
    #     self.assertEqual(pooling.member, pooling.location_from.member)
    #     self.assertEqual(pooling.member, pooling.location_to.member)


class ApplicationTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.pooler = member.factories.MemberFactory()
        cls.applicant1 = member.factories.MemberFactory()
        cls.applicant2 = member.factories.MemberFactory()

    def test_end_flow(self):
        application = factories.ApplicationFactory(status=ApplicationStatus.PENDING)
        self.assertFalse(application.end_flow(),
                         '{0} is not an endflow.'.format(ApplicationStatus.get_name(application.status)))

        application = factories.ApplicationFactory(status=ApplicationStatus.ACCEPTED)
        self.assertFalse(application.end_flow(),
                         '{0} is not an endflow.'.format(ApplicationStatus.get_name(application.status)))

        application = factories.ApplicationFactory(status=ApplicationStatus.CANCELED)
        self.assertTrue(application.end_flow(),
                         '{0} should be an endflow.'.format(ApplicationStatus.get_name(application.status)))

        application = factories.ApplicationFactory(status=ApplicationStatus.REJECTED)
        self.assertTrue(application.end_flow(),
                         '{0} should be an endflow.'.format(ApplicationStatus.get_name(application.status)))

        application = factories.ApplicationFactory(status=ApplicationStatus.EXPIRED)
        self.assertTrue(application.end_flow(),
                         '{0} should be an endflow.'.format(ApplicationStatus.get_name(application.status)))

    def test_get_next_statuses(self):
        pass

    def test_has_correct_status(self):
        application = factories.ApplicationFactory(status=ApplicationStatus.PENDING)
        self.assertTrue(application.is_pending())

        application = factories.ApplicationFactory(status=ApplicationStatus.ACCEPTED)
        self.assertTrue(application.was_accepted())

        application = factories.ApplicationFactory(status=ApplicationStatus.EXPIRED)
        self.assertTrue(application.has_expired())

        application = factories.ApplicationFactory(status=ApplicationStatus.CANCELED)
        self.assertTrue(application.was_canceled())

        application = factories.ApplicationFactory(status=ApplicationStatus.REJECTED)
        self.assertTrue(application.was_rejected())

    def test_was_sent_by_me(self):
        application = factories.ApplicationFactory(member=ApplicationTest.applicant1)
        client = Client()
        client.login(user=self.applicant1.username, password=ApplicationTest.applicant1.username)
        self.assertTrue(application.was_sent_by_me())


class ApplicationStatusTest(unittest.TestCase):

    def test_next_accepted_statuses(self):
        expected = [ApplicationStatus.ACCEPTED, ApplicationStatus.CANCELED, ApplicationStatus.EXPIRED]
        actual = ApplicationStatus.next_1_statuses()
        self.assertItemsEqual(expected, actual)

    def test_next_rejected_statuses(self):
        expected = [ApplicationStatus.REJECTED]
        actual = ApplicationStatus.next_5_statuses()
        self.assertItemsEqual(expected, actual)

    def test_next_pending_statuses(self):
        expected = [ApplicationStatus.PENDING, ApplicationStatus.ACCEPTED, ApplicationStatus.CANCELED,
                    ApplicationStatus.EXPIRED, ApplicationStatus.REJECTED]
        actual = ApplicationStatus.next_10_statuses()
        self.assertItemsEqual(expected, actual)

    def test_next_pending_applicant_statuses(self):
        expected = [ApplicationStatus.PENDING, ApplicationStatus.CANCELED, ApplicationStatus.EXPIRED]
        actual = ApplicationStatus.next_10_applicant_statuses()
        self.assertItemsEqual(expected, actual)

    def test_next_pending_receiver_statuses(self):
        expected = [ApplicationStatus.PENDING, ApplicationStatus.ACCEPTED, ApplicationStatus.EXPIRED,
                    ApplicationStatus.REJECTED]
        actual = ApplicationStatus.next_10_receiver_statuses()
        self.assertItemsEqual(expected, actual)

    def test_next_canceled_statuses(self):
        expected = [ApplicationStatus.CANCELED]
        actual = ApplicationStatus.next_15_statuses()
        self.assertItemsEqual(expected, actual)

    def test_next_expired_statuses(self):
        expected = [ApplicationStatus.EXPIRED]
        actual = ApplicationStatus.next_20_statuses()
        self.assertItemsEqual(expected, actual)
