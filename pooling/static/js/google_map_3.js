var map;
var marker;
var marker_from, marker_to;
var circle_from, circle_to;
var infowindow_from, infowindow_to;
var pooling_marker_from, pooling_marker_to;

$(document).ready(function() {
    function initialize() {
        var mapOptions = {
            zoom: 12,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        geocoder = new google.maps.Geocoder();
        map = new google.maps.Map(document.getElementById('div_map_canvas'), mapOptions);
        google.maps.event.trigger(map, "resize");

        // Try HTML5 geolocation
        if(navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                function(position) {
                    var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

                    google.maps.event.trigger(map, "resize");
                    marker = new google.maps.Marker({
                        position: pos,
                        map: map,
                        title: "Ubicación actual"
                    });
                    map.setCenter(pos);
                    updateFrom();
                    updateTo();
                },
                function() {
                    handleNoGeolocation(true);
                }
            );
        } else {
            // Browser doesn't support Geolocation
            handleNoGeolocation(false);
        }

     /*   var center = map.getCenter();
        google.maps.event.trigger(map, "resize");
        map.setCenter(center); */
    }

    function handleNoGeolocation(errorFlag) {
        var content;
        if (errorFlag) {
            content = 'Error: The Geolocation service failed.';
        } else {
            content = 'Error: Your browser doesn\'t support geolocation.';
        }

        var options = {
            map: map,
            position: new google.maps.LatLng(60, 105),
            content: content
        };
        map.setCenter(options.position);
    }

    function addInfoWindow(infowindow, marker, elem_id) {
        if (infowindow != undefined) {
            infowindow.close();
        }
        if (marker != undefined) {
            var title;
            if (elem_id.toLowerCase().indexOf("from") != -1) {
                title = "Desde";
            } else {
                title = "Hasta";
            }
            var description = $("#" + elem_id + " option:selected").text();
            if (description == undefined || description.length == 0) {
                description = $("#" + elem_id).text();
            }
            marker.setTitle(title);
            infowindow = new google.maps.InfoWindow({
                content: "<h4>" + title + "</h4>" + description,
                maxWidth: 200
            });
            infowindow.open(map, marker);
        }
    }

    function updateFrom() {
        var location_id = $("#id_location_from option:selected").val();
        if (location_id == undefined) {
            location_id = $("#id_location_from").val() || $("#id_location_from").attr("data-value");
        }
        if (location_id != undefined && location_id.length > 0) {
            var distance = $('#id_max_distance_from').val();
            distance = parseInt(distance);
            $.get("/poolings/locations/json-detail/" + location_id,
                function(data) {
                    var position = new google.maps.LatLng(data.latitude, data.longitude);
                    if (marker_from == undefined) {
                        marker_from = new google.maps.Marker({
                            position: position,
                            map: map,
                            icon: "http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png"
                        });
                        marker_from.setPosition(position);
                        google.maps.event.addListener(marker_from, 'click', function() {
                            addInfoWindow(infowindow_from, marker_from, 'id_location_from');
                        });
                    } else {
                        marker_from.setPosition(position);
                        addInfoWindow(infowindow_from, marker_from, 'id_location_from');
                    }
                    if (map != undefined) {
                        map.setCenter(position);
                    }
                    var radiusOptions = {
                        strokeColor: '#0066cc',
                        strokeOpacity: '0.8',
                        strokeWeight: 2,
                        fillColor: '#0066ff',
                        fillOpacity: 0.35,
                        map: map,
                        center: position,
                        radius: distance
                    };
                    if (circle_from != undefined)
                        circle_from.setMap(null);
                    circle_from = new google.maps.Circle(radiusOptions);
                }
            );
        }
    }

    function updateTo() {
        var location_id = $("#id_location_to option:selected").val();
        if (location_id == undefined) {
            location_id = $("#id_location_to").val() || $("#id_location_to").attr("data-value");
        }
        if (location_id != undefined && location_id.length > 0) {
            var distance = $('#id_max_distance_to').val();
            distance = parseInt(distance);
            $.get("/poolings/locations/json-detail/" + location_id,
                function(data) {
                    var position = new google.maps.LatLng(data.latitude, data.longitude);
                    if (marker_to == undefined) {
                        marker_to = new google.maps.Marker({
                            position: position,
                            map: map,
                            icon: "http://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png"
                        });
                        marker_to.setPosition(position);
                        google.maps.event.addListener(marker_to, 'click', function() {
                            addInfoWindow(infowindow_to, marker_to, 'id_location_to');
                        });
                    } else {
                        marker_to.setPosition(position);
                        addInfoWindow(infowindow_to, marker_to, 'id_location_to');
                    }
                    if (map != undefined) {
                        map.setCenter(position);
                    }
                    var radiusOptions = {
                        strokeColor: '#009900',
                        strokeOpacity: '0.8',
                        strokeWeight: 2,
                        fillColor: '#009933',
                        fillOpacity: 0.35,
                        map: map,
                        center: position,
                        radius: distance
                    };
                    if (circle_to != undefined)
                        circle_to.setMap(null);
                    circle_to = new google.maps.Circle(radiusOptions);
                }
            );
        }
    }

    function showSearchOnMap(event) {
        $.get('/poolings/json-detail/' + $(event.target).attr('data-id'),
            function(data) {
                var position = new google.maps.LatLng(data.location_from_latitude, data.location_from_longitude);
                if (pooling_marker_from == undefined) {
                    pooling_marker_from = new google.maps.Marker({
                        position: position,
                        map: map,
                        icon: "http://www.google.com/intl/en_us/mapfiles/ms/micons/yellow-dot.png"
                    });
                    pooling_marker_from.setPosition(position);
                } else {
                    pooling_marker_from.setPosition(position);
                }
                position = new google.maps.LatLng(data.location_to_latitude, data.location_to_longitude);
                if (pooling_marker_to == undefined) {
                    pooling_marker_to = new google.maps.Marker({
                        position: position,
                        map: map,
                        icon: "http://www.google.com/intl/en_us/mapfiles/ms/micons/yellow-dot.png"
                    });
                    pooling_marker_to.setPosition(position);
                } else {
                    pooling_marker_to.setPosition(position);
                }
            }
        );
    }

    google.maps.event.addDomListener(window, 'load', initialize);

    $("#button_map").on('click', function() { relocateMap(); });
    $("#id_location_from").on('change', function(event) {
        updateFrom();
    });
    $("#id_location_to").on('change', function(event) {
        updateTo();
    });
    $("#id_max_distance_from").on('change', function(event) {
        updateFrom();
    });
    $("#id_max_distance_to").on('change', function(event) {
        updateTo();
    });
    $(".class_view_on_map").on('click', function(event) {
        showSearchOnMap(event);
    });

    updateFrom();
    updateTo();
});
