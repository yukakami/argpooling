var map;
var marker;
var geocoder;
$(document).ready(function() {
    function initialize() {
        var mapOptions = {
            zoom: 13,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        geocoder = new google.maps.Geocoder();
        map = new google.maps.Map(document.getElementById('div_map_canvas'), mapOptions);
        google.maps.event.trigger(map, "resize");

        var latitude = $("#id_latitude").val();
        var longitude = $("#id_longitude").val();
        if (latitude != undefined && latitude.length > 0 && longitude != undefined && longitude.length > 0) {
            updateMarker(undefined, latitude, longitude);
        } else {
            // Try HTML5 geolocation
            if(navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(
                    function(position) {
                        var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                        var infowindow = new google.maps.InfoWindow({
                            map: map,
                            position: pos
                        });
                        google.maps.event.trigger(map, "resize");
                        updateMarker(pos);
                    },
                    function() {
                        handleNoGeolocation(true);
                    }
                );
            } else {
                // Browser doesn't support Geolocation
                handleNoGeolocation(false);
            }
        }

     /*   var center = map.getCenter();
        google.maps.event.trigger(map, "resize");
        map.setCenter(center); */
    }

    function handleNoGeolocation(errorFlag) {
        if (errorFlag) {
            var content = 'Error: The Geolocation service failed.';
        } else {
            var content = 'Error: Your browser doesn\'t support geolocation.';
        }

        var options = {
            map: map,
            position: new google.maps.LatLng(60, 105),
            content: content
        };
        var infowindow = new google.maps.InfoWindow(options);
        map.setCenter(options.position);
    }

    function relocateMap() {
        var city = $("#id_city_text").val();
        var street = $("#id_street").val();
        if (street == undefined)
            street = "";
        else
            street += ", ";
        if (city != undefined && city.length > 0) {
            var address = street + city;
            geocoder.geocode({'address': address}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    updateMarker(results[0].geometry.location);
                } else if (status == google.maps.GeocoderStatus.ZERO_RESULTS) {
                    alert("Nothing found.");
                } else {
                    alert("Another status.");
                }
            });
        }
    }

    function updateMarker(position, latitude, longitude) {
        if (position == undefined) {
            position = new google.maps.LatLng(latitude, longitude);
        }
        if (marker == undefined) {
            marker = new google.maps.Marker({
                position: position,
                map: map,
                draggable: true
            });
            google.maps.event.addListener(marker, 'dragend', function() {
                updateLatLng(marker.getPosition());
                markerDragend(marker.getPosition());
            });
        }
        map.setCenter(position);
        marker.setPosition(position);
        updateLatLng(position);
    }

    function updateLatLng(position) {
        $("#id_latitude").val(position.lat());
        $("#id_longitude").val(position.lng());
        console.log("acá tengo que actualizar la ciudad también...");
    }

    function markerDragend(position) {
        // locality or administrative_area_level_2 = city
        // administrative_area_level_1 = province
        // country = country
        // route
        // street_number
        geocoder.geocode({latLng: position}, function(response) {
            if (response && response.length > 0) {
                var address_components = response[0].address_components;
                var street = "";
                var city = "";
                var province = "";
                var country = "";
                var component;
                for (var i = 0; i < address_components.length; i++) {
                    console.log("markerDragend", address_components);
                    component = address_components[i];
                    if (component.types[0] == 'route')
                        street = component.long_name + street;
                    else if (component.types[0] == 'street_number')
                        street = street + " " + component.long_name;
                    else if (component.types[0] == 'country')
                        country = component.long_name;
                    else if (component.types[0] == 'administrative_area_level_1')
                        province = component.long_name;
                    else if (component.types[0] == 'administrative_area_level_2' && city.length == 0)
                        city = component.long_name;
                    else if (component.types[0] == 'locality')
                        city = component.long_name;
                }
                city = city.replace('Department', '');
                city = city.replace('Partido', '');
                province = province.replace('Province', '');
                $("#id_street").val(street);
                $("#id_city_text").val(city + ", " + province + ", " + country);
                $.ajax({
                    url: '/regions/cities/json-get-or-create/',
                    type: 'GET',
                    data: {q: $("#id_city_text").val()},
                    success: function(data) {
                        $("#id_city").val(data['city'].id);
                    }
                });
            }
        });
    }

    google.maps.event.addDomListener(window, 'load', initialize);


    $("#button_map").on('click', function() { relocateMap(); });
});
