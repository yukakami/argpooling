var map;
var marker;
var marker_from, marker_to;
var infowindow_from, infowindow_to;

$(document).ready(function() {
function initialize() {
    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    geocoder = new google.maps.Geocoder();
    map = new google.maps.Map(document.getElementById('div_map_canvas'), mapOptions);
    google.maps.event.trigger(map, "resize");
      
    // Try HTML5 geolocation
    if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
            function(position) {
                var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

                google.maps.event.trigger(map, "resize");
                marker = new google.maps.Marker({
                    position: pos,
                    map: map,
                    title: "Ubicación actual"
                });                    
                map.setCenter(pos);
updateFrom();
updateTo();
            }, 
            function() {
                handleNoGeolocation(true);
            }
        );
    } else {
        // Browser doesn't support Geolocation
        handleNoGeolocation(false);
    }

 /*   var center = map.getCenter();
    google.maps.event.trigger(map, "resize");
    map.setCenter(center); */
}

function handleNoGeolocation(errorFlag) {
    if (errorFlag) {
        var content = 'Error: The Geolocation service failed.';
    } else {
        var content = 'Error: Your browser doesn\'t support geolocation.';
    }

    var options = {
        map: map,
        position: new google.maps.LatLng(60, 105),
        content: content
    };
    map.setCenter(options.position);
}

    function addInfoWindow(infowindow, marker, elem_id) {
        if (infowindow != undefined) {
            infowindow.close();
        }
        if (marker != undefined) {
            var title;
            if (elem_id.toLowerCase().indexOf("from") != -1) {
                title = "Desde";
            } else {
                title = "Hasta";
            }
            var description = $("#" + elem_id + " option:selected").text();
            if (description == undefined || description.length == 0) {
                description = $("#" + elem_id).text();
            }
            marker.setTitle(title);
            infowindow = new google.maps.InfoWindow({
                content: "<h4>" + title + "</h4>" + description,
                maxWidth: 200
            });
            infowindow.open(map, marker);
        }
    }

function updateFrom() {
    var location_id = $("#id_location_from option:selected").val();
    if (location_id == undefined) {
        location_id = $("#id_location_from").val() || $("#id_location_from").attr("data-value");
    }
    if (location_id != undefined && location_id.length > 0) {
        $.get("/poolings/locations/json-detail/" + location_id, 
            function(data) {
                var position = new google.maps.LatLng(data.latitude, data.longitude);
                if (marker_from == undefined) {
                    marker_from = new google.maps.Marker({
                        position: position,
                        map: map,
                        icon: "http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png"
                    });
                    marker_from.setPosition(position);
                    google.maps.event.addListener(marker_from, 'click', function() {
                        addInfoWindow(infowindow_from, marker_from, 'id_location_from');
                    });
                } else {
                    marker_from.setPosition(position);
                    addInfoWindow(infowindow_from, marker_from, 'id_location_from');
                }
                if (map != undefined) {
                    map.setCenter(position);
                }
            }
        );
    }
}

function updateTo() {
    var location_id = $("#id_location_to option:selected").val();
    if (location_id == undefined) {
        location_id = $("#id_location_to").val() || $("#id_location_to").attr("data-value");
    }
    if (location_id != undefined && location_id.length > 0) {
        $.get("/poolings/locations/json-detail/" + location_id, 
            function(data) {
                var position = new google.maps.LatLng(data.latitude, data.longitude);
                if (marker_to == undefined) {
                    marker_to = new google.maps.Marker({
                        position: position,
                        map: map,
                        icon: "http://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png"
                    });
                    marker_to.setPosition(position);
                    google.maps.event.addListener(marker_to, 'click', function() {
                        addInfoWindow(infowindow_to, marker_to, 'id_location_to');
                    });
                } else {
                    marker_to.setPosition(position);
                    addInfoWindow(infowindow_to, marker_to, 'id_location_to');
                }
                map.setCenter(position);
            }
        );
    }
}

google.maps.event.addDomListener(window, 'load', initialize);

$("#button_map").on('click', function() { relocateMap(); });
$("#id_location_from").on('change', function(event) {
    updateFrom();
});
$("#id_location_to").on('change', function(event) {
    updateTo();
});
});
