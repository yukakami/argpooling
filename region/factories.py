__author__ = 'yukakami'
import factory.django
from region.models import Country, Province, City


class CountryFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Country

    name = factory.Sequence(lambda c: 'Country_{0}'.format(c))


class ProvinceFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Province

    name = factory.Sequence(lambda c: 'Province_{0}'.format(c))
    country = factory.SubFactory(CountryFactory)


class CityFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = City

    name = factory.Sequence(lambda c: 'City_{0}'.format(c))
    province = factory.SubFactory(ProvinceFactory)
