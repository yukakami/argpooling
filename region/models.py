from django.db import models


class Country (models.Model):
    name = models.CharField(max_length=100)
    codename = models.CharField(max_length=2, blank=True, null=True)
    phone_code = models.IntegerField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        ordering = ['name']

    def __unicode__(self):
        return unicode(self.name)


class Province (models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True, null=True)
    country = models.ForeignKey(Country)

    class Meta:
        ordering = ['name', 'country']

    def __unicode__(self):
        return unicode("%s, %s" % (self.name, self.country.name))


class City (models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True, null=True)
    zipcode = models.CharField(max_length=50, null=True, blank=True)
    province = models.ForeignKey(Province)

    class Meta:
        ordering = ['name', 'province']

    def __unicode__(self):
        return unicode("%s, %s" % (self.name, self.province))
