__author__ = 'yukakami'

from django.conf.urls import patterns, url, include
from views import *


cities_urls = patterns('',
                       url(r'^json-list', CityListJsonView.as_view(), name='json-list'),
                       url(r'^json-get-or-create', CityGetOrCreateJsonView.as_view(), name='json-get-or-create'),)

urlpatterns = patterns('',
                       url(r'^cities/', include(cities_urls, namespace='city')), )
