from django.views.generic import TemplateView
from django.db.models import Q
from generic.views import JsonResponseMixin
from models import City, Province, Country


class CityListJsonView(JsonResponseMixin, TemplateView):

    def get_context_data(self, **kwargs):
        q = self.request.GET.get('q', None)
        if q:
            chunks = q.split(',')
            if len(chunks) == 1:
                chunk_city = chunks[0].strip()
                cities = City.objects.filter(Q(name__icontains=chunk_city) | Q(province__name__icontains=chunk_city))
            elif len(chunks) == 2:
                chunk_city = chunks[0].strip()
                chunk_province = chunks[1].strip()
                cities = City.objects.filter(Q(name__icontains=chunk_city), Q(province__name__icontains=chunk_province))
            elif len(chunks) == 3:
                chunk_city = chunks[0].strip()
                chunk_province = chunks[1].strip()
                chunk_country = chunks[2].strip()
                cities = City.objects.filter(Q(name__icontains=chunk_city), Q(province__name__icontains=chunk_province),
                                             Q(province__country__name__icontains=chunk_country))
        else:
            cities = City.objects.all()
        suggestions = []
        for city in cities:
            suggestions.append({'value': unicode(city), 'data': city.pk})
        return {'suggestions': suggestions}

    def render_to_response(self, context, **response_kwargs):
        return self.render_to_json_response(context, **response_kwargs)


class CityGetOrCreateJsonView(JsonResponseMixin, TemplateView):

    def get_context_data(self, **kwargs):
        q = self.request.GET.get('q', None)
        city = None
        if q:
            chunks = q.split(',')
            if len(chunks) == 3:
                chunk_city = chunks[0].strip()
                chunk_province = chunks[1].strip()
                chunk_country = chunks[2].strip()
                try:
                    city = City.objects.get(Q(name__icontains=chunk_city), Q(province__name__icontains=chunk_province),
                                            Q(province__country__name__icontains=chunk_country))
                except City.DoesNotExist:
                    country, was_created = Country.objects.get_or_create(name=chunk_country.title())
                    province, was_created = Province.objects.get_or_create(name=chunk_province.title(), country=country)
                    city, was_created = City.objects.get_or_create(name=chunk_city.title(), province=province)
            return {'city': {'id': city.id}}
        return {}

    def render_to_response(self, context, **response_kwargs):
        return self.render_to_json_response(context, **response_kwargs)
