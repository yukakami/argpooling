from tastypie.resources import ModelResource
from models import City


class CityResource(ModelResource):

    class Meta:
        queryset = City.objects.all()
        fields = ['id']
