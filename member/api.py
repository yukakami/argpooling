__author__ = 'yukakami'

from django.contrib.auth.models import User

from tastypie.resources import ModelResource
from tastypie import fields

from models import Member, Contact


class ContactResource(ModelResource):

    class Meta:
        queryset = Contact.objects.filter(public=True)
        excludes = ['public']


class UserResource(ModelResource):

    class Meta:
        queryset = Member.objects.all()


class MemberResource(ModelResource):
    user = fields.ForeignKey(UserResource, 'user')

    class Meta:
        queryset = Member.objects.all()
        fields = ['user__username']