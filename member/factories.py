__author__ = 'yukakami'
import factory
from member.models import Member


class MemberFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Member

    username = factory.Sequence(lambda c: 'username_{0}'.format(c))
    password = factory.SelfAttribute('username')
    email = factory.Sequence(lambda c: 'username_{0}@mail.com'.format(c))

    @classmethod
    def _prepare(cls, create, **kwargs):
        password = kwargs.pop('password', None)
        user = super(MemberFactory, cls)._prepare(create, **kwargs)
        if password:
            user.set_password(password)
            if create:
                user.save()
        return user
