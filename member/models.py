from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.utils import timezone
from django.utils.translation import ugettext as _


class ContactValidation(object):
    EMAIL = 1
    URL = 2
    PHONE = 3
    OTHER = 10

    @classmethod
    def get_choices(cls):
        choices = [
            (cls.EMAIL, _('e-mail'.capitalize())), (cls.URL, _('url'.capitalize())),
            (cls.OTHER, _('other'.capitalize())), (cls.PHONE, _('phone number'.capitalize()))
        ]
        return choices


class GenderChoices(object):
    FEMALE = 'F'
    MALE = 'M'

    @classmethod
    def get_choices(cls):
        choices = [(cls.FEMALE, _('female'.capitalize())), (cls.MALE, _('male'.capitalize()))]
        return choices

    @classmethod
    def get_name(cls, value):
        if cls.FEMALE == value:
            name = 'woman'.capitalize()
        elif cls.MALE == value:
            name = 'man'.capitalize()
        else:
            name = 'undefined'.capitalize()
        return name


class ContactType(models.Model):
    name = models.CharField(max_length=50, unique=True)
    description = models.TextField(blank=True, null=True)
    validation = models.IntegerField(choices=ContactValidation.get_choices(), default=ContactValidation.OTHER)

    class Meta:
        verbose_name = _('contact type')

    def __unicode__(self):
        return self.name


class Contact(models.Model):
    type = models.ForeignKey(ContactType)
    value = models.CharField(max_length=100)
    availability = models.TextField(verbose_name=_('describe availability'), blank=True, null=True)
    public = models.BooleanField(default=False, help_text=_('Make this contact information public or not.'))

    class Meta:
        verbose_name = _('contact')

    def __unicode__(self):
        return '%s: %s' % (self.type, self.value)


class MemberManager(BaseUserManager):

    def create_user(self, username, email, password=None):
        if not username:
            raise ValueError(_('the username is mandatory'))
        user = self.model(username=username, email=BaseUserManager.normalize_email(email))
        user.set_password(password)
        user.is_active = True
        user.is_superuser = False
        user.save(using=self._db)
        return user

    def create_superuser(self, username, email, password=None):
        user = self.create_user(username, email, password)
        user.is_admin = True
        user.is_active = True
        user.is_superuser = True
        user.is_staff = True
        user.save(using=self._db)
        return user


def content_filename(instance, filename):
    return '/'.join(['member_photo', 'member_%s' % instance.user.id, filename])


class Member(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(max_length=25, unique=True, verbose_name=_("username"))
    slug = models.SlugField(max_length=25, blank=True)
    first_name = models.CharField(max_length=30, blank=True, null=True, verbose_name=_("first name"))
    last_name = models.CharField(max_length=30, blank=True, null=True, verbose_name=_("last name"))
    email = models.EmailField(verbose_name=_("email"))
    contact_informations = models.ManyToManyField(Contact, blank=True, null=True)
    birthdate = models.DateField(help_text=_("the birthdate will be hidden"), null=True, blank=True,
                                 verbose_name=_("birthdate"))
    gender = models.CharField(max_length=2, choices=GenderChoices.get_choices(), null=True, blank=True,
                              help_text=_("the gender will be hidden"), verbose_name=_("gender"))
    is_admin = models.BooleanField(blank=True, default=False)

    objects = MemberManager()

    REQUIRED_FIELDS = ('email', )
    USERNAME_FIELD = 'username'

    class Meta:
        verbose_name = _('member')

    def __unicode__(self):
        return unicode(self.username)

    @property
    def get_gender(self):
        return GenderChoices.get_name(self.gender)

    @property
    def is_staff(self):
        return self.is_admin

    def get_name(self):
        return self.get_full_name() or self.username

    def get_full_name(self):
        if self.first_name is not None and len(self.first_name) > 0 and \
                self.last_name is not None and len(self.last_name) > 0:
            return "%s %s" % (self.first_name, self.last_name)
        return None

    def get_short_name(self):
        return self.username

    @property
    def public_contact_informations(self):
        return self.contact_informations.filter(public=True)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        self.slug = self.username
        return super(Member, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                        update_fields=update_fields)
