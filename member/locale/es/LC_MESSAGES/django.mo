��    +      t  ;   �      �     �  ,   �            	        %     ,     5     E     M     Z     p     x  
     	   �     �  	   �     �     �  
   �  
   �  %   �     �     �     �               !     *     3     8     =     \     l     �     �     �  '   �                 %   *  k  P     �  &   �       	             2  
   ;     F     Z     c     t  	   �     �     �     �     �     �     �     �  	   �     �  *   �     	     	  
   (	     3	     H	  	   ]	  
   g	     r	     z	      �	     �	     �	     �	  #   �	     
  +   ;
     g
     y
  	   �
  (   �
                     $                 (                                         	                 "   %      !                 
       +      &                         '       #   *      )                     %(object.username)s's profile Make this contact information public or not. Members' list accepted birthdate cancel canceled change password contact contact type describe availability expired female first name full name gender last name male member my profile new member no applications were sent or received other password pending phone number pooling's chart received rejected save sent sent and received applications statistic chart the birthdate will be hidden the gender will be hidden the username is mandatory there are no elements to show. to change the password, follow the link username view my profile view profile your profile was updated successfully Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-04-08 20:15+0000
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 perfil de %(object.username)s Hacer esta información pública o no. Lista de miembros aceptadas fecha de cumpleaños cancelar canceladas cambiar contraseña contacto tipo de contacto describir disponibilidad expiradas mujer nombre nombre completo género apellido hombre miembro mi perfil nuevo miembro ninguna aplicación fue enviada o recibida otro contraseña pendientes número de teléfono gráfico de poolings recibidas rechazadas guardar enviadas solicitudes enviadas y recibidas gráfico de estadísticas el cumpleaños será escondido el género será escondido el nombre de usuario es obligatorio no hay elementos para mostrar. para cambiar la contraseña, siga el enlace nombre de usuario ver mi perfil mi perfil su perfil se ha actualizado exitosamente 