MemberModel = Backbone.Model.extend({
    /*
     * Url de acceso al api rest para obtener registros de tipo Persona.
     */
    urlRoot: 'api/v1/member/'
});