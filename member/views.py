from django.views.generic import ListView, UpdateView, DetailView
from django.conf import settings
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.db.models import Q
from django.utils.translation import ugettext as _
from models import Member
from forms import ProfileForm
from generic.views import LoginRequiredView
from pooling.models import Application, ApplicationStatus


class MemberListView(LoginRequiredView, ListView):
    model = Member
    context_object_name = 'member_list'
    paginate_by = getattr(settings, 'PAGINATE_BY', 20)


class MemberDetailView(LoginRequiredView, DetailView):
    model = Member

    def get_context_data(self, **kwargs):
        user = self.object
        context_data = super(MemberDetailView, self).get_context_data(**kwargs)
        sent_application_count = Application.objects.filter(member=user).count()
        received_application_count = Application.objects.filter(pooling__member=user).count()
        sent_received_applications = Application.objects.filter(Q(pooling__member=user) | Q(member=user))
        context_data['sent_application_count'] = sent_application_count
        context_data['received_application_count'] = received_application_count
        context_data['total_application_count'] = received_application_count + sent_application_count
        context_data['accepted_application_count'] = sent_received_applications.filter(status_statistic=ApplicationStatus.ACCEPTED).count()
        context_data['rejected_application_count'] = sent_received_applications.filter(status_statistic=ApplicationStatus.REJECTED).count()
        context_data['canceled_application_count'] = sent_received_applications.filter(status_statistic=ApplicationStatus.CANCELED).count()
        context_data['expired_application_count'] = sent_received_applications.filter(status_statistic=ApplicationStatus.EXPIRED).count()
        context_data['pending_application_count'] = sent_received_applications.filter(status_statistic=ApplicationStatus.PENDING).count()
        return context_data


class MemberUpdateView(LoginRequiredView, UpdateView):
    model = Member
    form_class = ProfileForm
    template_name = 'member/profile_form.html'

    def get_object(self, queryset=None):
        username = self.request.user.username
        return Member.objects.get(username=username)

    def get_success_url(self):
        return reverse('account_profile')

    def form_valid(self, form):
        messages.add_message(self.request, messages.constants.SUCCESS, _(u"your profile was updated successfully"))
        return super(MemberUpdateView, self).form_valid(form)
