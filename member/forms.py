from django.contrib.auth import get_user_model
from django import forms
from models import Member
from generic import widgets


class RegistrationForm(forms.ModelForm):
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    account_field_names = ('username', 'email', 'password1', 'password2')
    personal_field_names = ('first_name', 'last_name', 'gender', 'birthdate')

    class Meta:
        model = get_user_model()
        fields = ('first_name', 'last_name', 'gender', 'birthdate', 'username', 'email', 'password1', 'password2')
        widgets = {'birthdate': widgets.DateWidget}

    def __init__(self, *args, **kwargs):
        super(RegistrationForm, self).__init__(*args, **kwargs)

    def signup(self, request, user):
        user.save()


class ProfileForm(forms.ModelForm):

    class Meta:
        model = Member
        exclude = ('is_staff', 'is_active', 'is_superuser', 'date_joined', 'last_login', 'groups',
                   'user_permissions', 'username', 'password')
        widgets = {'birthdate': widgets.DateWidget}

    def clean_first_name(self):
        first_name = self.cleaned_data.get('first_name', '')
        return first_name.title()

    def clean_last_name(self):
        last_name = self.cleaned_data.get('last_name', '')
        return last_name.title()
