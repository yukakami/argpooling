__author__ = 'yukakami'

from django.conf.urls import patterns, include, url
from tastypie.api import Api

from api import MemberResource, ContactResource
from views import MemberListView, MemberDetailView


v1_api = Api(api_name='v1')
v1_api.register(MemberResource())
v1_api.register(ContactResource())

urlpatterns = patterns('',
                url(r'^api/', include(v1_api.urls)),
                url(r'^list/', MemberListView.as_view(), name='list'),
                url(r'^(?P<slug>[\w-]+)/', MemberDetailView.as_view(), name='detail'),
)
